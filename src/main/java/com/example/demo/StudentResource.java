package com.example.demo;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class StudentResource {	
	
	@PostMapping("/student")
	public ResponseEntity<String> getStudent(@RequestBody int s) {
		
		if(s%3==0 && s%5==0) {
			return new ResponseEntity<String>("AB", HttpStatus.OK);
		}
		
		if(s%3==0) {
			return new ResponseEntity<String>("A", HttpStatus.OK);
		}
		if(s%5==0) {
			return new ResponseEntity<String>("B", HttpStatus.OK);
		}
		
		return null;
	}
	
	@GetMapping("/hello")
	public String getDetails() {
		return "welcome";
	}

}
